public class Operations {

    int desiredNumber = 10;

    /**
     * pass by value esample
     * @param desiredNumber
     */
    void updatedValue(int desiredNumber){
        desiredNumber = desiredNumber + 20;
    }

    /**
     * pass by reference example
     * return adding 20 to previous value
     * @param op
     */
    void updatedValue(Operations op){
        op.desiredNumber = (op.desiredNumber + 20);
    }
}

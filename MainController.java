

public class MainController {

    public static void main(String[] args) {
       
        /**
         * pass by value example
         */
        Operations op = new Operations();
        System.out.println("(pass by value)Value before changing data: "+op.desiredNumber);
        op.updatedValue(30);
        System.out.println("(pass by value)Value after changing data: "+op.desiredNumber);

        /**
         * pass by reference example
         */
        Operations op1 = new Operations();
        System.out.println("(pass by reference)Value before changing data: "+op1.desiredNumber);
        op.updatedValue(op1);
        System.out.println("(pass by reference)Value after changing data: "+op1.desiredNumber);


    }

   
}
